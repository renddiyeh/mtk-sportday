const orange = '#f89a1c';
const green = '#50b848';
const gray = '#666666';
const about = {
  yellow: '#fedd00',
  blue: '#29ABE2',
  pink: '#E83891',
  green: '#50B848',
};

export default {
  orange,
  green,
  gray,
  primary: orange,
  secondary: green,
  about,
};
