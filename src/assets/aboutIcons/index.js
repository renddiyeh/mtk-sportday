import colors from '@/colors';

import games from './games.svg';
import info from './info.svg';
import rules from './rules.svg';
import scores from './scores.svg';

export default {
  games: {
    color: colors.about.yellow,
    icon: games,
  },
  info: {
    color: colors.about.blue,
    icon: info,
  },
  fair: {
    color: colors.about.pink,
    icon: rules,
  },
  traffic: {
    color: colors.about.green,
    icon: scores,
  },
};
