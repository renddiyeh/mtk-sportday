// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueYouTubeEmbed from 'vue-youtube-embed';
import 'sanitize.css';
import smothScroll from 'smoothscroll-polyfill';

import App from './App';
import store from './store';
import router from './router';

smothScroll.polyfill();

Vue.config.productionTip = false;
Vue.use(VueYouTubeEmbed);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
