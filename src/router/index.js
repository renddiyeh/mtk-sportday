import Vue from 'vue';
import Router from 'vue-router';
import ga from 'vue-ga';
import HomePage from '@/pages/HomePage';
import NewsPage from '@/pages/NewsPage';
import AboutPage from '@/pages/AboutPage';
import AlbumPage from '@/pages/AlbumPage';

import store from '../store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomePage,
    },
    {
      path: '/news/:slug?',
      name: 'News',
      component: NewsPage,
    },
    {
      path: '/about/:slug?',
      name: 'About',
      component: AboutPage,
    },
    {
      path: '/album/:slug?',
      name: 'Album',
      component: AlbumPage,
    },
  ],
});

router.afterEach((from, to) => {
  if (from.name !== to.name) window.scroll({ top: 0, left: 0 });
  store.commit('closeMenu');
});

ga(router, 'UA-58674730-13');

export default router;
