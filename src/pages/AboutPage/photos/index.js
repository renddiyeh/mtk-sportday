import map from './map.png';
import parking from './parking.png';
import fair1 from './New_fair-1.png';
import fair2 from './fair-2.png';
import fair3 from './fair-3.png';
import table1 from './table1.png';
import table2 from './table2.png';
import table3 from './table3.png';
import table4 from './table4.png';
import table5 from './table5.png';
import table6 from './table6.png';
import table7 from './table7.png';
import table8 from './table8.png';
import eventmap from './new_eventmap2.png';

export default {
  map,
  parking,
  fair1,
  fair2,
  fair3,
  table1,
  table2,
  table3,
  table4,
  table5,
  table6,
  table7,
  table8,
  eventmap,
};
