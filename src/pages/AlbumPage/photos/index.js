import img1 from './1028-1.png';
import img2 from './1028-2.png';
import img3 from './1028-3.png';
import img4 from './1028-4.png';
import img5 from './1028-5.png';
import img6 from './edm01-01_0908_Rev.jpg';
import img7 from './edm02-01_09082017_v1.jpg';
import img8 from './edm04-01_09222017.jpg';
import img9 from './edm05-01_10052017.jpg';
import img10 from './edm06-01_10062017.jpg';
import img11 from './edm09-1-01.jpg';
import img12 from './edm09-2-01.jpg';
import img13 from './edm09-2-02.jpg';
import img14 from './IMG_9633.jpg';
import starsVideo from './stars.mp4';
import stars from './stars.png';
import img15 from './1.jpg';
import img16 from './2.jpg';
import img17 from './3.jpeg';
import img18 from './4.jpg';
import img19 from './5.jpg';
import img20 from './6.jpg';
import img21 from './7.jpg';
import img22 from './8.jpg';
import img23 from './9.jpg';
import img24 from './10.jpg';
import img25 from './11.jpg';
import img26 from './12.jpg';
import img27 from './Album_2.jpg';
import img28 from './20週年運動會中英文版手冊.png';
import img29 from './1101.jpg';
import img30 from './1102.jpg';
import img31 from './1103.png';
import eventVideo from './20th.mp4';
import eventpng from './20th.png';
import eventVideo2 from './20161116-ok_1.mp4';
import event2png from './20161116-ok.png';
import eventVideo3 from './video-3.mp4';
import event3png from './video-3.png';

export default[
  {
    width: 418,
    height: 629,
    src: img1,
    slug: 'img1',
    text: '新台灣之光林子偉即將現身運動會，現場安排互動遊戲拿禮物及開放拍照時間，邀請大家同歡!',
  },
  {
    width: 418,
    height: 629,
    src: img2,
    slug: 'img2',
    text: '有氧運動界最FUN的潘若迪老師將在運動會帶領大家一起動起來，晚來就跟不上囉!',
  },
  {
    width: 418,
    height: 629,
    src: img3,
    slug: 'img3',
    text: '小朋友最愛看的表演都在小舞台區! 月亮姐姐將帶來精采的親子帶動唱，親子同樂就在運動會。',
  },
  {
    width: 418,
    height: 629,
    src: img4,
    slug: 'img4',
    text: '小朋友最愛看的表演都在小舞台區! 不讓月亮姐姐獨占鰲頭，風靡小朋友界的YoYo Man 也不缺席喔!',
  },
  {
    width: 800,
    height: 800,
    src: img5,
    slug: 'img5',
    text: 'Passion Sisters 不只在棒球賽中出現，也將在運動會現場綻放熱力!',
  },
  {
    width: 1295,
    height: 766,
    src: img6,
    slug: 'img6',
  },
  {
    width: 3000,
    height: 2250,
    src: img7,
    slug: 'img7',
  },
  {
    width: 3000,
    height: 2250,
    src: img8,
    slug: 'img8',
  },
  {
    width: 3000,
    height: 2250,
    src: img9,
    slug: 'img9',
  },
  {
    width: 3000,
    height: 2250,
    src: img10,
    slug: 'img10',
  },
  {
    width: 3000,
    height: 2250,
    src: img11,
    slug: 'img11',
  },
  {
    width: 3000,
    height: 2250,
    src: img12,
    slug: 'img12',
  },
  {
    width: 3000,
    height: 2250,
    src: img13,
    slug: 'img13',
  },
  {
    width: 3000,
    height: 2250,
    src: img14,
    slug: 'img14',
  },
  {
    width: 1920,
    height: 1080,
    src: stars,
    slug: 'stars',
    video: starsVideo,
    text: '聯發科運動會大明星say HI',
  },
  {
    width: 720,
    height: 960,
    src: img15,
    slug: 'img15',
  },
  {
    width: 1080,
    height: 1440,
    src: img16,
    slug: 'img16',
  },
  {
    width: 3024,
    height: 4032,
    src: img17,
    slug: 'img17',
  },
  {
    width: 720,
    height: 960,
    src: img18,
    slug: 'img18',
  },
  {
    width: 1231,
    height: 1217,
    src: img19,
    slug: 'img19',
  },
  {
    width: 1800,
    height: 1355,
    src: img20,
    slug: 'img20',
  },
  {
    width: 2048,
    height: 1536,
    src: img21,
    slug: 'img21',
  },
  {
    width: 2364,
    height: 1774,
    src: img22,
    slug: 'img22',
  },
  {
    width: 2048,
    height: 1536,
    src: img23,
    slug: 'img23',
  },
  {
    width: 3264,
    height: 2448,
    src: img24,
    slug: 'img24',
  },
  {
    width: 4032,
    height: 3024,
    src: img25,
    slug: 'img25',
  },
  {
    width: 960,
    height: 720,
    src: img26,
    slug: 'img26',
  },
  {
    width: 3000,
    height: 2250,
    src: img27,
    slug: 'img27',
    text: '一目了然場地配置圖',
  },
  {
    width: 312,
    height: 313,
    src: img28,
    slug: 'img28',
    text: '掃描QR Code,立即下載20週年運動會中英文版手冊',
  },
  {
    width: 1280,
    height: 960,
    src: img29,
    slug: 'img29',
  },
  {
    width: 1280,
    height: 960,
    src: img30,
    slug: 'img30',
  },
  {
    width: 1280,
    height: 960,
    src: img31,
    slug: 'img31',
  },
  {
    width: 1920,
    height: 1080,
    src: eventpng,
    slug: 'eventVideo',
    video: eventVideo,
    text: '',
  },
  {
    width: 1920,
    height: 1080,
    src: event2png,
    slug: 'eventVideo2',
    video: eventVideo2,
    text: '',
  },
  {
    width: 1920,
    height: 1080,
    src: event3png,
    slug: 'eventVideo3',
    video: eventVideo3,
    text: '20週年運動會完整全紀錄，帶你重溫精彩的每一刻！',
  },
];
