import img1 from './News_1.jpg';
import img2 from './News_2.jpg';


export default {
  slug: 'news6',
  date: '2017-11-08',
  title: '20週年的運動會只有這一次',
  content: `聯發科技20週年運動會盛大舉辦！
  專屬星光陣容，與蕭敬騰、朱俐靜及瑤瑤共享星空下的音樂饗宴。
  超豐富園遊會，美食饗宴，超過130家南北美食及人氣名店入駐。
  更別忘了運動會的初衷，多元趣味與正規賽事，享受與團隊一起揮灑汗水，共享榮耀的珍貴時刻。 (當加油團也很酷!)`,
  images: [
    img1, img2,
  ],
};
