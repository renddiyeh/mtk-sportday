import img1 from './news_03.jpg';

export default {
  slug: 'news5',
  date: '2017-10-18',
  title: '<運動好物商城> 折扣碼專區',
  content: `聯發科技20週年運動會同仁專屬20組折扣碼攏底家!
  每人20組七折折扣碼，每筆最多可折抵NT$1500，使用期限至 2017/12/31呦!
  GLGJHE6A   |   ZLLQLTNX   |   XE63A7Q9   |   X788DJJU  
  RGWC5QUL   |   ZLNPCNK7   |   SSZSFJL4   |   6QTATRSJ
  X5HWS6SH   |   5MZ4G3TJ   |   AXHGAMKU   |   PZYDZGNG  
  LK9YCP5X   |   ZKPWQCCK   |   BHEMLVUX   |   9YJFQ5WU  
  FEAV9UQU   |   7JZF6VEA   |   H8UWEMHN   |   UMHBGSRQ  
`,
  images: [
    img1,
  ],
};
