import news1 from './news1';
import news2 from './news2';
import news3 from './news3';
import news4 from './news4';
import news5 from './news5';
import news6 from './news6';


export default [
  news1,
  news2,
  news3,
  news4,
  news5,
  news6,
].map(item => Object.assign({}, item, {
  content: item.content.replace(/\n/g, '\n\n'),
})).reverse();
