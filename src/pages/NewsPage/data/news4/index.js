import img1 from './news_03.jpg';

export default {
  slug: 'news4',
  date: '2017-10-17',
  title: '<快閃活動> 拍照上傳拿好禮',
  content: `聯發科技20週年運動會快閃拍照活動開跑囉!
  10/20起於各廠區開跑，詳細時間及地點請見下表:
  - 10/20 (Fri) 11:30-13:00 新竹AB棟穿堂
  - 10/23 (Mon) 11:30-13:00 新竹C棟 & 內湖辦公室 lobby
  - 10/24 (Tue) 11:30-13:00 新竹E棟 & 行善辦公室 lobby
  - 10/25 (Wed) 11:30-13:00 新竹AB棟穿堂 & 竹北辦公室 lobby
  每日限量50個驚喜小禮，歡迎大家一起來快閃拍照，與世界分享充滿創意的聨發人照片呦!
  
`,
  images: [
    img1,
  ],
};
