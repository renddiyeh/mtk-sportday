const ghpages = require('gh-pages');

ghpages.publish('dist', {
  branch: 'build',
}, (err) => {
  if (err) console.error(err);
});
